package backend

import (
	"flag"
	"log"
	"net/http"
	"time"

	"bitbucket.org/sti-covid-2021/app/src/backend/logic"
	"github.com/gorilla/mux"
)

func HTTPRouter(ip string, port string) {
	var dir string

	flag.StringVar(&dir, dir, "./src/static/", "the directory to serve files from. Defaults to the current dir")
	flag.Parse()
	r := mux.NewRouter()
	api := r.PathPrefix("/api/v1").Subrouter()
	api.HandleFunc("/health", logic.HealthCheck).Methods(http.MethodGet)
	api.HandleFunc("/mzcr/today", mzcr_today).Methods(http.MethodGet)
	api.HandleFunc("/mzcr/days", mzcr_days).Methods(http.MethodGet)
	api.HandleFunc("/who/days", who_days).Methods(http.MethodGet)
	api.HandleFunc("/who/vaccinations", who_vaccinations).Methods(http.MethodGet)
	api.HandleFunc("/request_update", logic.RequestUpdate).Methods(http.MethodGet)

	srv := &http.Server{
		Handler: r,
		Addr:    ip + port,
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	log.Fatal(srv.ListenAndServe())
}

func mzcr_today(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "src/app_data/mzcr_today.json")
}
func mzcr_days(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "src/app_data/mzcr_days.json")
}
func who_days(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "src/app_data/who_global.json")
}
func who_vaccinations(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "src/app_data/who_vaccine.json")
}
