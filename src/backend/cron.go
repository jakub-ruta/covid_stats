package backend

import (
	"fmt"
	"time"

	"bitbucket.org/sti-covid-2021/app/src/backend/logic"
	"github.com/go-co-op/gocron"
)

func RegisterCron() {
	fmt.Println("registering cron")
	s2 := gocron.NewScheduler(time.UTC)
	s2.Every(2).Hours().Do(logic.UpdateLocalData)
	s2.StartAsync()
}
