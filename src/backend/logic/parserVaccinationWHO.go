package logic

import (
	"encoding/csv"
	"encoding/json"
	"log"
	"os"
)

type WHO_vaccine_raw struct {
	COUNTRY                              string `json:"Country"`
	ISO3                                 string `json:"Iso3"`
	WHO_REGION                           string `json:"Who_region"`
	DATA_SOURCE                          string `json:"Data_source"`
	DATE_UPDATED                         string `json:"Date_updated"`
	TOTAL_VACCINATIONS                   string `json:"Total_vaccinations"`
	PERSONS_VACCINATED_1PLUS_DOSE        string `json:"Persons_vaccinated_1plus_dose"`
	TOTAL_VACCINATIONS_PER100            string `json:"Total_caccinations_per100"`
	PERSONS_VACCINATED_1PLUS_DOSE_PER100 string `json:"Persons_vaccinated_1plus_dose_per100"`
	VACCINES_USED                        string `json:"Vaccines_used"` // all vaccines used
	FIRST_VACCINE_DATE                   string `json:"First_vaccine_date"`
	NUMBER_VACCINES_TYPES_USED           string `json:"Number_vaccines_types_used"`
}

type File_structure_WHO_vaccine_days struct {
	Rows []WHO_vaccine_raw `json:"data"`
}

func ParseWhoVaccine(input_path string) {
	log.Print("Parsing vaccination data who from " + input_path)
	csv_file, err := os.Open(input_path)
	if err != nil {
		log.Panic(err)
		return
	}

	if err != nil {
		log.Panic(err)
		return
	}

	defer csv_file.Close()
	r := csv.NewReader(csv_file)
	records, err := r.ReadAll()
	if err != nil {
		log.Panic(err)
		return
	}

	var who_vaccine File_structure_WHO_vaccine_days
	var who_row WHO_vaccine_raw

	for _, rec := range records {
		who_row.COUNTRY = rec[0]
		who_row.ISO3 = rec[1]
		who_row.WHO_REGION = rec[2]
		who_row.DATA_SOURCE = rec[3]
		who_row.DATE_UPDATED = rec[4]
		who_row.TOTAL_VACCINATIONS = rec[5]
		who_row.PERSONS_VACCINATED_1PLUS_DOSE = rec[6]
		who_row.TOTAL_VACCINATIONS_PER100 = rec[7]
		who_row.PERSONS_VACCINATED_1PLUS_DOSE_PER100 = rec[8]
		who_row.VACCINES_USED = rec[9] // all vaccines used
		who_row.FIRST_VACCINE_DATE = rec[10]
		who_row.NUMBER_VACCINES_TYPES_USED = rec[11]
		who_vaccine.Rows = append(who_vaccine.Rows, who_row)
	}
	// Convert to JSON
	json_data, err := json.Marshal(who_vaccine)
	if err != nil {
		log.Panic(err)
		return
	}

	//fmt.Println(string(json_data))

	//create json file for today
	//////////
	f, err := os.OpenFile("./src/app_data/who_vaccine.json", os.O_RDWR|os.O_CREATE, 0755)
	//f, err := os.OpenFile("../STIN13052021/app/src/app_data/who_vaccine.json", os.O_RDWR|os.O_CREATE, 0755)
	//////////
	if err != nil {
		log.Panic(err)
	}
	if _, err := f.Write([]byte(json_data)); err != nil {
		f.Close() // ignore error; Write error takes precedence
		log.Panic(err)
	}
	if err := f.Close(); err != nil {
		log.Panic(err)
	}
}

// testovací funkce (nastav na řádku 1 package main)
/*
func main() {
	ParseWhoVaccine("../STIN04052021/app/src/backend/logic/vaccination-data-deletethis.csv")
}
*/
