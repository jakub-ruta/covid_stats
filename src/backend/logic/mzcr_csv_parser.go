package logic

import (
	"encoding/csv"
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
	"time"
)

type MZCR_raw struct {
	Date                           string `json:"date"`
	AllTests                       string `json:"all_tests"`
	AllConfirmedCases              string `json:"all_confirmed_cases"`
	ActiveCases                    string `json:"active_cases"`
	Cured                          string `json:"cured"`
	Deaths                         string `json:"deaths"`
	InHospital                     string `json:"in_hospital"`
	YesterdayTests                 string `json:"yesterday_tests"`
	YesterdayCases                 string `json:"yesterday_cases"`
	TodayCases                     string `json:"today_cases"`
	YesterdayTestsDate             string `json:"yesterday_tests_date"`
	YesterdayCasesDate             string `json:"yesterday_cases_date"`
	TodayTestsDate                 string `json:"today_tests_date"`
	TodayCasesDate                 string `json:"today_cases_date"`
	AllAGTests                     string `json:"all_AG_tests"`
	YesterdayAGTests               string `json:"yesterday_AG_tests"`
	YesterdayAGTestsDate           string `json:"yesterday_AG_tests_date"`
	AllVaccinationsByYesterday     string `json:"all_vaccintaions_by_yesterday"`
	AllVaccinationsByYesterdayDate string `json:"all_vaccintaions_by_yesterday_date"`
	ActiveCases65                  string `json:"active_cases_65"`
	YesterdayActiveCases65         string `json:"yesterday_active_cases_65"`
	YesterdayActiveCases65Date     string `json:"yesterday_active_cases_65_date"`
}

type MZCR_today struct {
	Date              string `json:"date"`
	AllTests          string `json:"all_tests"`
	AllConfirmedCases string `json:"all_confirmed_cases"`
	ActiveCases       string `json:"active_cases"`
	Cured             string `json:"cured"`
	Deaths            string `json:"deaths"`
	InHospital        string `json:"in_hospital"`
	TodayCases        string `json:"today_cases"`
	AllAGTests        string `json:"all_AG_tests"`
	ActiveCases65     string `json:"active_cases_65"`
	ParsedAt          int64  `json:"parsed_at"`
}
type MZCR_day struct {
	Tests          string `json:"tests"`
	ConfirmedCases string `json:"confirmed_cases"`
	AgTests        string `json:"AG_tests"`
	Vaccinations   string `json:"vaccinations"`
	Cases65        string `json:"cases_65"`
	ParsedAt       int64  `json:"parsed_at"`
}
type MZCR_days struct {
	Date string   `json:"date"`
	Data MZCR_day `json:"data"`
}

type File_structure_MZCR_days struct {
	Rows []MZCR_days `json:"data"`
}

func ParseMZCR(input_path string) {
	log.Print("Parsing mzcr from " + input_path)
	csv_file, err := os.Open(input_path)
	if err != nil {
		log.Panic(err)
		return
	}

	if err != nil {
		log.Panic(err)
		return
	}
	defer csv_file.Close()
	r := csv.NewReader(csv_file)
	records, err := r.ReadAll()
	if err != nil {
		log.Panic(err)
		return
	}

	var mzcr MZCR_raw

	for _, rec := range records {
		mzcr.Date = rec[0]
		mzcr.AllTests = rec[1]
		mzcr.AllConfirmedCases = rec[2]
		mzcr.ActiveCases = rec[3]
		mzcr.Cured = rec[4]
		mzcr.Deaths = rec[5]
		mzcr.InHospital = rec[6]
		mzcr.YesterdayTests = rec[7]
		mzcr.YesterdayCases = rec[8]
		mzcr.TodayCases = rec[9]
		mzcr.YesterdayTestsDate = rec[10]
		mzcr.YesterdayCasesDate = rec[11]
		mzcr.TodayTestsDate = rec[12]
		mzcr.TodayCasesDate = rec[13]
		mzcr.AllAGTests = rec[14]
		mzcr.YesterdayAGTests = rec[15]
		mzcr.YesterdayAGTestsDate = rec[16]
		mzcr.AllVaccinationsByYesterday = rec[17]
		mzcr.AllVaccinationsByYesterdayDate = rec[18]
		mzcr.ActiveCases65 = rec[19]
		mzcr.YesterdayActiveCases65 = rec[20]
		mzcr.YesterdayActiveCases65Date = rec[21]

	}

	var today MZCR_today
	today.ActiveCases = mzcr.ActiveCases
	today.ActiveCases65 = mzcr.ActiveCases65
	today.AllAGTests = mzcr.AllAGTests
	today.AllConfirmedCases = mzcr.AllConfirmedCases
	today.AllTests = mzcr.AllTests
	today.Cured = mzcr.Cured
	today.Date = mzcr.Date
	today.Deaths = mzcr.Deaths
	today.InHospital = mzcr.InHospital
	now := time.Now()
	today.ParsedAt = now.Unix()
	today.TodayCases = mzcr.TodayCases

	var yesterday MZCR_day
	yesterday.AgTests = mzcr.YesterdayAGTests
	yesterday.Cases65 = mzcr.YesterdayActiveCases65
	yesterday.ConfirmedCases = mzcr.YesterdayCases
	yesterday.ParsedAt = now.Unix()
	yesterday.Tests = mzcr.YesterdayTests
	yesterday.Vaccinations = mzcr.AllVaccinationsByYesterday

	var days_strucutre MZCR_days
	days_strucutre.Date = mzcr.AllVaccinationsByYesterdayDate
	days_strucutre.Data = yesterday

	// Convert to JSON
	json_data, err := json.Marshal(today)
	if err != nil {
		log.Panic(err)
		return
	}
	/** Unused

	yesterday_json, err := json.Marshal(yesterday)
	if err != nil {
		log.Panic(err)
		return
	}*/
	//print json data
	//fmt.Println(string(json_data))
	//fmt.Println(string(yesterday_json))

	//create json file for today

	f, err := os.OpenFile("./src/app_data/mzcr_today.json", os.O_RDWR|os.O_CREATE, 0755)
	if err != nil {
		log.Panic(err)
	}
	if _, err := f.Write([]byte(json_data)); err != nil {
		f.Close() // ignore error; Write error takes precedence
		log.Panic(err)
	}
	if err := f.Close(); err != nil {
		log.Panic(err)
	}

	// read prev json data file
	file, _ := ioutil.ReadFile("./src/app_data/mzcr_days.json")

	data := File_structure_MZCR_days{
		Rows: []MZCR_days{},
	}

	_ = json.Unmarshal([]byte(file), &data)
	//fmt.Println("OLD JSON")
	//fmt.Println(data)
	var k = 0
	// add data to data structure TODO: not duplicated
	for index, element := range data.Rows {
		if element.Date == days_strucutre.Date {
			data.Rows[index] = days_strucutre
			k++
		}
	}
	if k == 0 {
		data.Rows = append(data.Rows, days_strucutre)
	}
	//fmt.Println()

	// create json file for days data
	days, err := json.Marshal(data)
	if err != nil {
		log.Panic(err)
		return
	}
	f_days, err := os.OpenFile("./src/app_data/mzcr_days.json", os.O_RDWR|os.O_CREATE, 0755)
	if err != nil {
		log.Panic(err)
	}
	if _, err := f_days.Write([]byte(days)); err != nil {
		f_days.Close() // ignore error; Write error takes precedence
		log.Panic(err)
	}
	if err := f_days.Close(); err != nil {
		log.Panic(err)
	}
}
