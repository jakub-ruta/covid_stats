package logic

import (
	"encoding/csv"
	"encoding/json"
	"log"
	"os"
)

type WHO_global_raw struct {
	Date_reported     string `json:"date_reported"`
	Country_code      string `json:"country_code"`
	Country           string `json:"country"`
	Who_region        string `json:"who_region"`
	New_cases         string `json:"new_cases"`
	Cumulative_cases  string `json:"cumulative_cases"`
	New_deaths        string `json:"new_deaths"`
	Cumulative_deaths string `json:"cumulative_deaths"`
}

type File_structure_WHO_global_days struct {
	Rows []WHO_global_raw `json:"data"`
}

func ParseWhoGlobal(input_path string) {
	log.Print("Parsing who from " + input_path)
	csv_file, err := os.Open(input_path)
	if err != nil {
		log.Panic(err)
		return
	}

	if err != nil {
		log.Panic(err)
		return
	}
	defer csv_file.Close()
	r := csv.NewReader(csv_file)
	records, err := r.ReadAll()
	if err != nil {
		log.Panic(err)
		return
	}

	var who_global File_structure_WHO_global_days
	var who_row WHO_global_raw

	for _, rec := range records {
		who_row.Date_reported = rec[0]
		who_row.Country_code = rec[1]
		who_row.Country = rec[2]
		who_row.Who_region = rec[3]
		who_row.New_cases = rec[4]
		who_row.Cumulative_cases = rec[5]
		who_row.New_deaths = rec[6]
		who_row.Cumulative_deaths = rec[7]
		who_global.Rows = append(who_global.Rows, who_row)
	}

	// Convert to JSON
	json_data, err := json.Marshal(who_global)
	if err != nil {
		log.Panic(err)
		return
	}
	//fmt.Println(string(json_data))

	//create json file for today

	f, err := os.OpenFile("./src/app_data/who_global.json", os.O_RDWR|os.O_CREATE, 0755)
	if err != nil {
		log.Panic(err)
	}
	if _, err := f.Write([]byte(json_data)); err != nil {
		f.Close() // ignore error; Write error takes precedence
		log.Panic(err)
	}
	if err := f.Close(); err != nil {
		log.Panic(err)
	}

}
