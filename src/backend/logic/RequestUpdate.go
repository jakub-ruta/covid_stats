package logic

import (
	"net/http"
)

func RequestUpdate(w http.ResponseWriter, r *http.Request) {
	UpdateLocalData()
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(`{"message": "Updating"}`))
}
