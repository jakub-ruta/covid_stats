package logic

import (
	"io"
	"log"
	"net"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strings"
	"time"
)

var (
	fileName    string
	fullURLFile string
)

/*
// testovací funkce (nastav na řádku 1 package main)
func main() {
	//var file *os.File = getFileFromWeb("https://covid19.who.int/WHO-COVID-19-global-data.csv", "Covid app/app/src/app_data") // změň název projektu
	//var file2 *os.File = getFileFromWeb("https://covid19.who.int/WHO-COVID-19-global-data.csv", "Covid app/app/src/app_data") // změň název projektu
	getFileFromWeb("https://covid19.who.int/WHO-COVID-19-global-data.csv", "Covid app/app/src/app_data")  // změň název projektu
	getFileFromWeb("https://covid19.who.int/who-data/vaccination-data.csv", "Covid app/app/src/app_data") // změň název projektu
}
*/

//funkce vrací pointer na stažený soubor
//fullURLFile = odkaz souboru na netu
//pathToDirectoryToSaveFile = cesta do složky kam se soubor uloží (název souboru se nastavuje automaticky)
func getFileFromWeb(fullURLFile string, pathToDirectoryToSaveFile string) (f *os.File) {
	log.Print("Downloading data from " + fullURLFile)
	// Build fileName from fullPath
	fileURL, err := url.Parse(fullURLFile)
	if err != nil {
		log.Panic(err)
	}
	path := fileURL.Path
	segments := strings.Split(path, "/")
	fileName = segments[len(segments)-1]

	// Create blank file
	filePath, _ := filepath.Abs("../" + pathToDirectoryToSaveFile + "/" + fileName)
	file, err := os.Create(filePath)
	if err != nil {
		log.Panic(err)
	}
	client := http.Client{
		CheckRedirect: func(r *http.Request, via []*http.Request) error {
			r.URL.Opaque = r.URL.Path
			return nil
		},
		Transport: &http.Transport{
			Dial: (&net.Dialer{
				Timeout:   30 * time.Second,
				KeepAlive: 30 * time.Second,
			}).Dial,
			TLSHandshakeTimeout:   10 * time.Second,
			ResponseHeaderTimeout: 10 * time.Second,
			ExpectContinueTimeout: 1 * time.Second,
		},
	}

	// Put content on file
	resp, err := client.Get(fullURLFile)
	if err != nil {
		log.Panic(err)
	}
	defer resp.Body.Close()
	io.Copy(file, resp.Body)
	defer file.Close()

	return file
}
