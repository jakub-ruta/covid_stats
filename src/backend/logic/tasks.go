package logic

import (
	"fmt"
)

func UpdateLocalData() {
	fmt.Println("updating data")
	getFileFromWeb("https://covid19.who.int/WHO-COVID-19-global-data.csv", "app/src/app_data")
	getFileFromWeb("https://covid19.who.int/who-data/vaccination-data.csv", "app/src/app_data")
	getFileFromWeb("https://onemocneni-aktualne.mzcr.cz/api/v2/covid-19/zakladni-prehled.csv", "app/src/app_data")
	fmt.Println("parsing data")
	ParseWhoGlobal("src/app_data/WHO-COVID-19-global-data.csv")
	ParseMZCR("src/app_data/zakladni-prehled.csv")
	ParseWhoVaccine("src/app_data/vaccination-data.csv")
}
