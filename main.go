package main

import (
	"fmt"
	"log"
	"os"

	"bitbucket.org/sti-covid-2021/app/src/backend"
)

func main() {
	defer handlePanic()
	createFiles()
	initApp()
}

func initApp() {

	fmt.Println("start cron")
	backend.RegisterCron()
	fmt.Println("start router")
	backend.HTTPRouter("127.0.0.1", ":8083")
}

func handlePanic() {
	if r := recover(); r != nil {
		fmt.Println("Recovering from panic:", r)
		initApp()
	}
}
func createFiles() {
	_, err := os.Stat("src/app_data")

	if os.IsNotExist(err) {
		errDir := os.MkdirAll("src/app_data", 0755)
		if errDir != nil {
			log.Fatal(err)
		}

	}
}
