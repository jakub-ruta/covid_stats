
# STI semestrální práce

It is a project for STI on TUL. It displays covid statistic and diference between global data and czech data

## Server start
http server can be start by 
```bash
go run main.go 
```
from *root* of project

 
## File structure
`main.go` is entry file and it call router from `/src/backend/router.go`

`router.go` serve static files (img, html, css, js) from dir `/src/static`

*api* will be under `localhost:port/api/v1/`

there is *health* check under `http://localhost:8083/api/v1/health`

### MZCR
`http://localhost:8083/api/v1/mzcr/days` for days data from mzcr
`http://localhost:8083/api/v1/mzcr/today` for today data from mzcr

### WHO
`http://localhost:8083/api/v1/who/days` for days data from who
`http://localhost:8083/api/v1/who/vaccinations` for vaccinations data from who


#### File structure `http://localhost:8083/api/v1/mzcr/days`


```javascript
{
   "data":[
      {
         "date":"2021-04-19",
         "data":{
            "tests":"20937",
            "confirmed_cases":"3301",
            "AG_tests":"2021-04-19",
            "vaccinations":"55085",
            "cases_65":"456",
            "parsed_at":1620760485
         }
      },
      {
         "date":"2021-05-13",
         "data":{
            "tests":"12967",
            "confirmed_cases":"1218",
            "AG_tests":"2021-05-13",
            "vaccinations":"85621",
            "cases_65":"149",
            "parsed_at":1621017329
         }
      }
    ]
}
```
Data is array of days that has been loaded from mzcr


#### File structure `http://localhost:8083/api/v1/mzcr/today`


```javascript
{
   "date":"2021-05-18",
   "all_tests":"7036440",
   "all_confirmed_cases":"1654219",
   "active_cases":"24405",
   "cured":"1599881",
   "deaths":"29933",
   "in_hospital":"1351",
   "today_cases":"0",
   "all_AG_tests":"238023",
   "active_cases_65":"259643",
   "parsed_at":1621358121
}
```


#### File structure `http://localhost:8083/api/v1/who/days`


```javascript
{
   "data":[
      {
         "date_reported":"﻿Date_reported",
         "country_code":"Country_code",
         "country":"Country",
         "who_region":"WHO_region",
         "new_cases":"New_cases",
         "cumulative_cases":"Cumulative_cases",
         "new_deaths":"New_deaths",
         "cumulative_deaths":"Cumulative_deaths"
      },
      {
         "date_reported":"2020-01-03",
         "country_code":"AF",
         "country":"Afghanistan",
         "who_region":"EMRO",
         "new_cases":"0",
         "cumulative_cases":"0",
         "new_deaths":"0",
         "cumulative_deaths":"0"
      }
   ]
}
```
Data is array of cases and deaths per day and country



#### File structure `http://localhost:8083/api/v1/who/vaccinations`

```javascript
{
   "data":[
      {
         "Country":"COUNTRY",
         "Iso3":"ISO3",
         "Who_region":"WHO_REGION",
         "Data_source":"DATA_SOURCE",
         "Date_updated":"DATE_UPDATED",
         "Total_vaccinations":"TOTAL_VACCINATIONS",
         "Persons_vaccinated_1plus_dose":"PERSONS_VACCINATED_1PLUS_DOSE",
         "Total_caccinations_per100":"TOTAL_VACCINATIONS_PER100",
         "Persons_vaccinated_1plus_dose_per100":"PERSONS_VACCINATED_1PLUS_DOSE_PER100",
         "Vaccines_used":"VACCINES_USED",
         "First_vaccine_date":"FIRST_VACCINE_DATE",
         "Number_vaccines_types_used":"NUMBER_VACCINES_TYPES_USED"
      },
      {
         "Country":"El Salvador",
         "Iso3":"SLV",
         "Who_region":"AMRO",
         "Data_source":"REPORTING",
         "Date_updated":"2021-05-07",
         "Total_vaccinations":"1074764",
         "Persons_vaccinated_1plus_dose":"950691",
         "Total_caccinations_per100":"16.57",
         "Persons_vaccinated_1plus_dose_per100":"14.657",
         "Vaccines_used":"AstraZeneca - AZD1222,Pfizer BioNTech - Comirnaty,SII - Covishield,Sinovac - CoronaVac",
         "First_vaccine_date":"2021-02-17",
         "Number_vaccines_types_used":"4"
      },
      {
         "Country":"Democratic Republic of the Congo",
         "Iso3":"COD",
         "Who_region":"AFRO",
         "Data_source":"REPORTING",
         "Date_updated":"2021-05-12",
         "Total_vaccinations":"8446",
         "Persons_vaccinated_1plus_dose":"8446",
         "Total_caccinations_per100":"0.009",
         "Persons_vaccinated_1plus_dose_per100":"0.009",
         "Vaccines_used":"SII - Covishield",
         "First_vaccine_date":"2021-04-19",
         "Number_vaccines_types_used":"1"
      }
   ]
}

```

data is array of vaccination per country
## WHO vaccinations
![WHO vaccinations](https://cdn.dkspeed.tk/g/tul_skola/who_vaccinations.PNG)

## WHO days
![WHO days](https://cdn.dkspeed.tk/g/tul_skola/who_days.PNG)

## MZCR today
![MZCR today](https://cdn.dkspeed.tk/g/tul_skola/mzcr_today.PNG)

## MZCR days
![MZCR days](https://cdn.dkspeed.tk/g/tul_skola/mzcr_day.PNG)