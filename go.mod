module bitbucket.org/sti-covid-2021/app

go 1.16

require (
	github.com/go-co-op/gocron v1.5.0 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/robfig/cron v1.2.0 // indirect
)
